/**
 * @aiq.webdesigner
 * This script requires AIQ Web Designer
*/
setShadowDOM(true);
navigateTo("https://demosite.appvance.net/");
var isAssertionTrue=assertContainsText("Clothing",link("Clothing"));
if (isAssertionTrue){
   takeWindowScreenshot();
}
(