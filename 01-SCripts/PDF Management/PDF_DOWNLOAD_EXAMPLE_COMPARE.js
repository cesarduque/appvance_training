/**
 * @aiq.webdesigner
 * This script requires AIQ Web Designer
*/
setShadowDOM(true);
navigateTo("https://eforms.com/bill-of-sale/fl/");
click(fallback(`link("PDF")`,
   `link("btn download-link flex-1 h-11 hover:no-underline text-center whitespace-normal")`,
   `link("https://eforms.com/download/2015/12/florida-vehicle-boat-bill-of-sale-82050.pdf")`,
   `byXPath('//main/div[2]/div[1]/div/div[2]/div/a')`,
   `byXPath('//*[@class="download-link btn h-11 flex-1 hover:no-underline whitespace-normal text-center"]')`,
   `byXPath('/html/body/main/div[2]/div/div/div[2]/div/a')`,
   `byXPath("/html[@class='scroll-smooth [scroll-padding-top:4rem] md:[scroll-padding-top:4.75rem] hydrated']/body[@class='document-template-default single single-document postid-6595 tk-proxima-nova antialiased bg-gray-eformsbg text-neutral-800 scroll-smooth']/main[@class='bg-white pb-10']/div[@class='min-h-[calc(100vh-11rem)]']/div[@class='container max-w-screen-lg flex flex-col md:flex-row md:items-start gap-6 md:gap-0']/div[@class='md:mb-6 max-w-none w-full md:w-2/3 md:pr-6 flex flex-col gap-6']/div[@class='w-full flex flex-col sm:flex-row gap-4']/div[@class='w-full sm:w-auto flex flex-row justify-between gap-4 flex-wrap']/a[@class='download-link btn h-11 flex-1 hover:no-underline whitespace-normal text-center']")`,
   `link({'target':'_blank'})`,
   `byJQuery('div[class="container max-w-screen-lg flex flex-col md:flex-row md:items-start gap-6 md:gap-0"] > div[class="md:mb-6 max-w-none w-full md:w-2/3 md:pr-6 flex flex-col gap-6"] > div[class="w-full flex flex-col sm:flex-row gap-4"] > div[class="w-full sm:w-auto flex flex-row justify-between gap-4 flex-wrap"] > a[class="download-link btn h-11 flex-1 hover:no-underline whitespace-normal text-center"]')`,
   `byJQuery('a[href="https://eforms.com/download/2015/12/florida-vehicle-boat-bill-of-sale-82050.pdf"]')`));
click(fallback(`link("Close")`,
   `link("fancybox-close fancybox-item")`,
   `link("javascript:;")`,
   `byXPath('//div[3]/div/div/a')`,
   `byXPath('//*[@class="fancybox-item fancybox-close"]')`,
   `byXPath('/html/body/div[3]/div/div/a')`,
   `byXPath("/html[@class='scroll-smooth [scroll-padding-top:4rem] md:[scroll-padding-top:4.75rem] hydrated fancybox-margin fancybox-lock']/body[@class='document-template-default single single-document postid-6595 tk-proxima-nova antialiased bg-gray-eformsbg text-neutral-800 scroll-smooth']/div[@class='fancybox-overlay fancybox-overlay-fixed']/div[@class='fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened']/div[@class='fancybox-skin']/a[@class='fancybox-item fancybox-close']")`,
   `byJQuery('a[title="Close"]')`,
   `byJQuery('body[class="document-template-default single single-document postid-6595 tk-proxima-nova antialiased bg-gray-eformsbg text-neutral-800 scroll-smooth"] > div[class="fancybox-overlay fancybox-overlay-fixed"] > div[class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened"] > div[class="fancybox-skin"] > a[class="fancybox-item fancybox-close"]')`,
   `byJQuery('.document-template-default.single.single-document.postid-6595.tk-proxima-nova.antialiased.bg-gray-eformsbg.text-neutral-800.scroll-smooth > .fancybox-overlay.fancybox-overlay-fixed > .fancybox-wrap.fancybox-desktop.fancybox-type-inline.fancybox-opened > .fancybox-skin > .fancybox-item.fancybox-close')`,
   `byJQuery('a[href="javascript:;"]')`));
click(fallback(`link("https://eforms.com/download/2015/12/florida-vehicle-boat-bill-of-sale-82050.pdf[1]")`,
   `link("PDF[1]")`,
   `byXPath("//div[@id='article_view']/article/table/tbody/tr[1]/td/p[2]/a")`,
   `byXPath('//*[text()="PDF"]')`,
   `byXPath('/html/body/main/div[2]/div[3]/article/table/tbody/tr/td/p[2]/a')`,
   `byXPath("id('article_view')/article[@class='prose prose-neutral max-w-none w-full md:w-2/3 md:pr-6']/table[@class='doc-table']/tbody[1]/tr[1]/td[1]/p[2]/a[1]")`,
   `byXPath('//tbody//tr[1]//td[1]//p//a[text()="PDF"]')`,
   `link({'rel':'noopener'})`));
wait(5000);
//click(fallback(`link("Close")`,
//   `link("fancybox-close fancybox-item")`,
//   `link("javascript:;")`,
//   `byXPath('//div[3]/div/div/a')`,
//   `byXPath('//*[@class="fancybox-item fancybox-close"]')`,
//   `byXPath('/html/body/div[3]/div/div/a')`,
//   `byXPath("/html[@class='scroll-smooth [scroll-padding-top:4rem] md:[scroll-padding-top:4.75rem] hydrated fancybox-margin fancybox-lock']/body[@class='document-template-default single single-document postid-6595 tk-proxima-nova antialiased bg-gray-eformsbg text-neutral-800 scroll-smooth']/div[@class='fancybox-overlay fancybox-overlay-fixed']/div[@class='fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened']/div[@class='fancybox-skin']/a[@class='fancybox-item fancybox-close']")`,
//   `byJQuery('a[title="Close"]')`,
//   `byJQuery('body[class="document-template-default single single-document postid-6595 tk-proxima-nova antialiased bg-gray-eformsbg text-neutral-800 scroll-smooth"] > div[class="fancybox-overlay fancybox-overlay-fixed"] > div[class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened"] > div[class="fancybox-skin"] > a[class="fancybox-item fancybox-close"]')`,
//   `byJQuery('.document-template-default.single.single-document.postid-6595.tk-proxima-nova.antialiased.bg-gray-eformsbg.text-neutral-800.scroll-smooth > .fancybox-overlay.fancybox-overlay-fixed > .fancybox-wrap.fancybox-desktop.fancybox-type-inline.fancybox-opened > .fancybox-skin > .fancybox-item.fancybox-close')`,
//   `byJQuery('a[href="javascript:;"]')`));
var fileDownloadedList=getDownloadedFiles();
log(fileDownloadedList);
var File01=fileDownloadedList.getJSONObject(0).getString("name");
log(File01);
var File02=fileDownloadedList.getJSONObject(1).getString("name");
log(File02);
var fullJson01=extract("PDF",getResponseDownload(File01),"document");
log(fullJson01);
var fullJson02=extract("PDF",getResponseDownload(File02),"document");
log(fullJson02);
assertContains(fullJson01,"HSMV 82050");
takeHtmlSnapshot();
