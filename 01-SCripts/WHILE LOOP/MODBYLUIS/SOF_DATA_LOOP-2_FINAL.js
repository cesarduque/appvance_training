/**
 * @aiq.webdesigner
 * This script requires AIQ Web Designer
*/
setShadowDOM(true);
readCSVArray(var $data = readCSVListHashMap("{ds}/../../../07-DPLs/Credentials_test.csv");
var counter=0;
var recordcounter=0;
var processed=0;
while (counter <$data.length){
   if ($data[counter].Office == '01' && $data[counter].State == 'Active'){
      //navigateTo("https://demosite.appvance.net/");
      navigateTo("https://demosite.appvance.net/login");
      setValue(fallback(`byXPath('//*[@id="spree_user_email"]')`,
   `emailbox("form-control")`,
   `byXPath('//*[@name="spree_user[email]"]')`,
   `emailbox("spree_user_email")`,
   `emailbox("spree_user[email]")`,
   `byXPath("//input[@id='spree_user_email']")`,
   `byXPath('/html/body/div[2]/div/div/div/div/div[2]/form/fieldset/div/input')`,
   `byXPath("id('spree_user_email')")`,
   `emailbox({'type':'email'})`,
   `byJQuery('#spree_user_email')`,
   `byJQuery('input[name="spree_user[email]"]')`),$data[counter].Login);
      setValue(fallback(`password("spree_user_password")`,
   `byXPath('//*[@id="spree_user_password"]')`,
   `password("form-control")`,
   `byXPath('//*[@name="spree_user[password]"]')`,
   `password("spree_user[password]")`,
   `byXPath("//input[@id='spree_user_password']")`,
   `byXPath('/html/body/div[2]/div/div/div/div/div[2]/form/fieldset/div[2]/input')`,
   `byXPath("id('spree_user_password')")`,
   `password({'tabindex':'2'})`,
   `byJQuery('#spree_user_password')`,
   `byJQuery('input[name="spree_user[password]"]')`),$data[counter].Password);
      click(fallback(`submit("btn btn-block btn-lg btn-success")`,
   `byXPath('//*[@name="commit"]')`,
   `submit("commit")`,
   `submit("Login")`,
   `byXPath("//fieldset[@id='password-credentials']/div[4]/input")`,
   `byXPath('//*[@class="btn btn-lg btn-success btn-block"]')`,
   `byXPath('/html/body/div[2]/div/div/div/div/div[2]/form/fieldset/div[4]/input')`,
   `byXPath("id('password-credentials')/div[@class='form-group']/input[@class='btn btn-lg btn-success btn-block']")`,
   `submit({'tabindex':'3'})`,
   `byJQuery('div[class="form-group"] > input[class="btn btn-lg btn-success btn-block"]')`,
   `byJQuery('#password-credentials > .form-group > .btn.btn-lg.btn-success.btn-block')`,
   `byJQuery('input[name="commit"]')`,
   `byJQuery('input[value="Login"]')`));
      click(fallback(`link(1, _in(list("nav-bar")))`,
   `link(1, _in(list({'id':'nav-bar'})))`,
   `link("/logout")`,
   `link("Logout")`,
   `byXPath("//ul[@id='nav-bar']/li[2]/a")`,
   `byXPath('//*[text()="Logout"]')`,
   `byXPath('/html/body/div/header/div/div/nav/ul/li[2]/a')`,
   `byXPath("id('nav-bar')/li[2]/a[1]")`,
   `byJQuery('a[href="/logout"]')`));
      var processed=processed + 1;
   }
   var counter=counter + 1;
   var recordcounter=recordcounter + 1;
   log(counter);
}
alert("Total Records + " + recordcounter);
alert("Processed Records = " + processed);
