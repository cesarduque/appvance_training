/**
 * @aiq.webdesigner
 * This script requires AIQ Web Designer
*/
navigateTo("https://demosite.appvance.net/");
click(bold("Home"));
var $headerColor=getCssValue(heading1("site-title"),"color");
assertEqual($headerColor,"rgb(0, 0, 0)");
var $homeFontWeight=getCssValue(bold("Home"),"text-align");
assertEqual($homeFontWeight,"left");