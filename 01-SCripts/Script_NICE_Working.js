/**
 * @aiq.testdesigner
 * This script requires AIQ Test Designer
*/
setVariablesIfNeeded('REPOSITORY/appvance_training./TIM_FILE.csv','HashDPL',0,'en_US');
setShadowDOM(true);
navigateTo("https://demosite.appvance.com/");
click(fallback(`span("Ruby on Rails Tote")`,
   `span("info")`,
   `span(0, _in(link("Ruby on Rails Tote")))`));
setValue(fallback(`numberbox("form-control title")`,
   `numberbox("quantity")`,
   `numberbox(0, _in(div("input-group")))`,
   `numberbox(0, _in(div("Add To Cart[1]")))`),"2");
click(fallback(`submit("btn btn-success[1]")`,
   `submit("add-to-cart-button")`,
   `submit("button")`,
   `submit("Add To Cart")`,
   `submit(0, _in(span("input-group-btn")))`,
   `submit(0, _in(span("Add To Cart")))`));
click(fallback(`link("continue")`,
   `link("/products")`,
   `link("Continue shopping")`,
   `link(0, _in(paragraph("clear_cart_link")))`,
   `link(0, _in(paragraph("or           Continue shopping")))`));
click(fallback(`span("Ruby on Rails Baseball Jersey")`,
   `span("info[2]")`,
   `span(0, _in(link("Ruby on Rails Baseball Jersey")))`));
setValue(fallback(`numberbox("form-control title")`,
   `numberbox("quantity")`,
   `numberbox(0, _in(div("input-group")))`,
   `numberbox(0, _in(div("Add To Cart[1]")))`),"2");
click(fallback(`submit("btn btn-success[1]")`,
   `submit("add-to-cart-button")`,
   `submit("button")`,
   `submit("Add To Cart")`,
   `submit(0, _in(span("input-group-btn")))`,
   `submit(0, _in(span("Add To Cart")))`));
navigateTo("https://demosite.appvance.com/cart?variant_id=21");
navigateTo("https://demosite.appvance.com/checkout/registration");
setValue(fallback(`emailbox("form-control title")`,
   `emailbox("order_email")`,
   `emailbox("order[email]")`),"fred@flintstone.com");
click(fallback(`submit("btn btn-block btn-lg btn-success[1]")`,
   `submit("commit[1]")`,
   `submit("Continue")`));
navigateTo("https://demosite.appvance.com/checkout");
setValue(fallback(`textbox("form-control")`,
   `textbox("order_bill_address_attributes_firstname")`,
   `textbox("order[bill_address_attributes][firstname]")`,
   `textbox(0, _in(paragraph("bfirstname")))`,
   `textbox(0, _in(paragraph("form-group")))`,
   `textbox(0, _in(paragraph("First Name*")))`),"Nemo");
setValue(fallback(`textbox("form-control[1]")`,
   `textbox("order_bill_address_attributes_lastname")`,
   `textbox("order[bill_address_attributes][lastname]")`,
   `textbox(0, _in(paragraph("blastname")))`,
   `textbox(0, _in(paragraph("form-group[1]")))`,
   `textbox(0, _in(paragraph("Last Name*")))`),"Nobody");
setValue(fallback(`textbox("form-control required")`,
   `textbox("order_bill_address_attributes_address1")`,
   `textbox("order[bill_address_attributes][address1]")`,
   `textbox(0, _in(paragraph("baddress1")))`,
   `textbox(0, _in(paragraph("form-group[2]")))`,
   `textbox(0, _in(paragraph("Street Address*")))`),"Filler");
setValue(fallback(`textbox("form-control[3]")`,
   `textbox("order_bill_address_attributes_city")`,
   `textbox("order[bill_address_attributes][city]")`,
   `textbox(0, _in(paragraph("bcity")))`,
   `textbox(0, _in(paragraph("form-group[4]")))`,
   `textbox(0, _in(paragraph("City*")))`),"Filler");
setSelected(fallback(`select("form-control required")`,
   `select("order_bill_address_attributes_state_id")`,
   `select("order[bill_address_attributes][state_id]")`,
   `select(0, _in(paragraph("bstate")))`,
   `select(0, _in(paragraph("form-group[6]")))`),"Massachusetts");
setValue(fallback(`textbox("form-control[4]")`,
   `textbox("order_bill_address_attributes_zipcode")`,
   `textbox("order[bill_address_attributes][zipcode]")`,
   `textbox(0, _in(paragraph("bzipcode")))`,
   `textbox(0, _in(paragraph("form-group[7]")))`,
   `textbox(0, _in(paragraph("Zip*")))`),"223399");
setValue(fallback(`telephonebox("form-control")`,
   `telephonebox("order_bill_address_attributes_phone")`,
   `telephonebox("order[bill_address_attributes][phone]")`,
   `telephonebox(0, _in(paragraph("bphone")))`,
   `telephonebox(0, _in(paragraph("form-group[8]")))`,
   `telephonebox(0, _in(paragraph("Phone*")))`),"123456789");
click(fallback(`submit("btn btn-lg btn-success")`,
   `submit("commit")`,
   `submit("Save and Continue")`,
   `submit(0, _in(div("form-buttons text-right well")))`));
navigateTo("https://demosite.appvance.com/checkout/update/address");
