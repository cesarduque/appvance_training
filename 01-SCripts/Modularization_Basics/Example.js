setShadowDOM(true);
navigateTo("https://demosite.appvance.net/");

// Click on the product panel using multiple fallback locators
click(fallback(
   `div(2, _in(div("product_1"))).xy(0.11, 0.58 )`,
   `div(2, _in(div({'id':'product_1'}))).xy(0.11, 0.58 )`,
   `div("panel-footer text-center").xy(0.11, 0.58 )`,
   `div("$15.99").xy(0.11, 0.58 )`,
   `byXPath("//div[@id='product_1']/div/div[2]").xy(0.11, 0.58 )`,
   `byXPath('//*[@class="panel-footer text-center"]').xy(0.11, 0.58 )`,
   `byXPath('/html/body/div[2]/div/div/div/div[2]/div/div/div[2]').xy(0.11, 0.58 )`,
   `byXPath("id('product_1')/div[@class='panel panel-default']/div[@class='panel-footer text-center']").xy(0.11, 0.58 )`,
   `byJQuery('div[class="row"] > div[class="col-md-3 col-sm-6 col-xs-6 product-list-item"] > div[class="panel panel-default"] > div[class="panel-footer text-center"]').xy(0.11, 0.58 )`,
   `byJQuery('#product_1 > .panel.panel-default > .panel-footer.text-center').xy(0.11, 0.58 )`
));

// Get the color of the price text
var priceColor = getCssValue(byXPath("//div[@class='panel-footer text-center']//span[@class='price selling lead']"), "color");

// Ensure the color is correct (update the expected color as needed)
assertEqual(priceColor, "rgb(51, 51, 51)");  // Change this value to the actual expected color
