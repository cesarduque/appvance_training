setVariablesIfNeeded("{ds}/dpl.csv", "HashDPL", 0);
addSmartTagsLibrary("{ds}/Saitama.stags");
setFailOnValidations(true);
prepareDomain($baseURL);
setFindOnlyVisible(true);
setFindOnlyEnabled(true);
selectMainFrame();
navigateTo($baseURL);
click(st_products('Home'));
