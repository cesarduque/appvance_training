navigateTo("https://demosite.appvance.com/");
click(span(any("Ruby on Rails Tote","info")));
setValue(numberbox(any("quantity","form-control title")),"2");
click(submit(any("add-to-cart-button","button","Add To Cart")));
navigateTo("https://demosite.appvance.com/cart?variant_id=1");
click(link(any("continue","/products","Continue shopping")));
click(span(any("info[2]","Ruby on Rails Baseball Jersey")));
setValue(numberbox(any("quantity","form-control title")),"2");
click(submit(any("add-to-cart-button","button","Add To Cart")));
click(submit(any("checkout-link","btn btn-lg btn-success","checkout","Checkout")));
setValue(emailbox(any("order_email","form-control title")),"Cesar@appvance.com");
click(submit(any("btn btn-block btn-lg btn-success[1]","Continue")));
setValue(textbox(any("order_bill_address_attributes_firstname","form-control")),"CEsar");
setValue(textbox(any("order_bill_address_attributes_lastname")),"Duque");
setValue(textbox(any("order_bill_address_attributes_address1","form-control required")),"123 Evergreen Terrace");
setValue(textbox(any("order_bill_address_attributes_address2")),"");
setValue(textbox(any("order_bill_address_attributes_city")),"Springfield");
setSelected(select(any("order_bill_address_attributes_state_id","form-control required")),"Massachusetts");
setValue(textbox(any("order_bill_address_attributes_zipcode")),"123456");
setValue(telephonebox(any("order_bill_address_attributes_phone","form-control")),"123456789");
click(submit(any("btn btn-lg btn-success","commit","Save and Continue")));
navigateTo("https://demosite.appvance.com/checkout/update/address");
