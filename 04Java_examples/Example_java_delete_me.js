navigateTo("https://demosite.appvance.com/");
click(span(any("Ruby on Rails Tote","info")));
click(submit(any("add-to-cart-button","button","Add To Cart")));
click(link(any("continue","/products","Continue shopping")));
navigateTo("https://demosite.appvance.com/cart?variant_id=1");
navigateTo("https://demosite.appvance.com/checkout/registration");
setValue(emailbox(any("order_email","form-control title")),"testmail@test.com");
click(submit(any("btn btn-block btn-lg btn-success[1]","Continue")));
